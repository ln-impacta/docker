# Docker

Docker Compose project to start a container with Nginx (port 80) with network connection with another container running MySQL.

## Running the project

With Docker started on your machine, just run the following command:
`docker-compose up`

This command will start two containers, one with Nginx and the other one with MySQL.
